package main;

import org.json.JSONObject;

/**
 * The class is responsible for creating the json message. Given the 3 inputs, it creates the Json message 
 * @author Ferguson
 * 
 * Assumptions:
 * 		If the Json message failed to be created, we didn't want to crash the entire program. 
 *
 */
public final class ErrorJsonMessage {

	public static void printErrorMessage(Satellite satellite, String component, Warning warning) {
		
		try {
			JSONObject errorMessage = new JSONObject();
			errorMessage.put("satelliteId", satellite.getId().intValue() );
			errorMessage.put("severity", warning.toString());
			errorMessage.put("component", component);
			errorMessage.put("timestamp", satellite.getIssueFoundTimestamp());
			System.out.println(errorMessage);
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}

	}
		
}
