package main;

import java.sql.Timestamp;

/**
 * This class is used to decide if a telemetry point is a problem or not.
 * @author Ferguson
 * 
 * Assumptions:
 * 		We only cared about the two warnings, red high and red low
 *
 */
public class StatLine {

	private Timestamp timestamp;
	private Warning warning;
	
	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Warning getWarning() {
		return warning;
	}

	public void setWarning(Warning warning) {
		this.warning = warning;
	}

	public StatLine(InputData inputData) {
		this.timestamp = inputData.getTimestamp();
		if (inputData.getRawValue() > inputData.getRedHighLimit()) {
			this.warning=Warning.RED_HIGH;
		}
		else if (inputData.getRawValue() < inputData.getRedLowLimit()) {
			this.warning=Warning.RED_LOW;
		}
		else {
			this.warning=Warning.NA;
		}
		
	}
}
