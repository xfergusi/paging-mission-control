package main;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 * This class is used to signify a satellite. It holds information regarding 2 components
 * The satellite class is responsible for storing as well as handling its telemetry data
 * issueFoundTimestamp was made to pass the timestamp needed for the error message
 * To create a satellite, all we need is an ID
 * @author Ferguson
 *
 * Assumptions:
 * 		We only have the two components(battery and thermostat). If we need more, a new design would be achieved to handle more.
 * 		
 * 		 
 */
public class Satellite {

	private Double id;
	private ArrayList<StatLine> batteryStats = new ArrayList<>();
	private ArrayList<StatLine> thermoStats = new ArrayList<>();
	private Timestamp issueFoundTimestamp;



	public Satellite(Double id) {
		this.id = id;
	}

	public void handleStatLine(StatLine statLine, String component) {
		switch(component) {
		case "BATT":
			if(statLine.getWarning().equals(Warning.RED_LOW)) {
				this.issueFoundTimestamp = ProblemChecker.checkForIssues(this.batteryStats, statLine.getTimestamp());
				this.batteryStats.add(statLine);

			}
		
		case "TSTAT":
			if(statLine.getWarning().equals(Warning.RED_HIGH)) {
				this.issueFoundTimestamp = ProblemChecker.checkForIssues(this.thermoStats, statLine.getTimestamp());
				this.thermoStats.add(statLine);
			}
		}
		
	}
	
	
	public Timestamp getIssueFoundTimestamp() {
		return issueFoundTimestamp;
	}

	public void setIssueFoundTimestamp(Timestamp issuesFound) {
		this.issueFoundTimestamp = issuesFound;
	}
	
	public Double getId() {
		return id;
	}

	public void setId(Double id) {
		this.id = id;
	}

	public List<StatLine> getBatteryStats() {
		return batteryStats;
	}

	public void setBatteryStats(ArrayList<StatLine> batteryStats) {
		this.batteryStats = batteryStats;
	}

	public List<StatLine> getThermoStats() {
		return thermoStats;
	}

	public void setThermoStats(ArrayList<StatLine> thermoStats) {
		this.thermoStats = thermoStats;
	}
	
	
	
	
}
