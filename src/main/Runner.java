package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Please see README for any details regarding the project.
 * 
 * Runner is main class that simply loops through the input files and executes functionality
 * 
 * @author Ferguson
 *
 */
public class Runner {

	public static void main(String[] args) throws IOException {
		
	     BufferedReader reader = new BufferedReader(new FileReader(args[0]));
	     SatelliteManager satelliteManager = new SatelliteManager();
	     
	     String currentLine;	     
	     while((currentLine = reader.readLine()) != null) {

	    	 InputData inputData = new InputData(currentLine.split("\\|"));
		     
		     StatLine statLine = new StatLine(inputData);
		    
		     Satellite satellite = satelliteManager.getSatellite(inputData.getSatelliteId());
		     satellite.handleStatLine(statLine, inputData.getComponent());
		     
		  
		     if(satellite.getIssueFoundTimestamp()!=null) {
		    	 ErrorJsonMessage.printErrorMessage(satellite, inputData.getComponent(), statLine.getWarning());
		    	 satellite.setIssueFoundTimestamp(null);
		     }
		     
	     }
	     
	     reader.close();
		
	}

}
