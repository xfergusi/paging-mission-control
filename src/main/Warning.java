package main;

/**
 * This is the enum for a warning.
 * @author Ferguson
 * 
 * Assumptions:
 * 		More errors would come, so enum would be a great was to keep track of them
 */
public enum Warning {
	RED_HIGH,
	RED_LOW,
	NA
}
