package main;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * This class is used for checking an issue found within the telemetry data
 * Rules being checked for at this time
 * If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
 * If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
 * The one method so happens to check for both of the issues above 
 * @author Ferguson
 *
 * Assumptions:
 * 		The rules above did not change. If they were to change, the check for issues method would have to be updated. Possible split out
 * 		A bit of extra logic was added to make sure I showed the first timestamp like the examples
 *
 */
public final class ProblemChecker {

	public static Timestamp checkForIssues(ArrayList<StatLine> stats, Timestamp timestamp) {
		ArrayList<Timestamp> correctTimestamp = new ArrayList<>();
		
		long counter = 0;
		
		//looping through all of the errors found on a satellite
		for(StatLine pivotStatLine : stats) {
			
			long timeForErrors = 5*60*1000;
			long fiveMinutesFromNewTS = timestamp.getTime()-timeForErrors;
			//the new timestamp - 5 minutes would give me the range I want
			//if I find any older timestamps that were given to me within that range, then we found a problem
			if(fiveMinutesFromNewTS<=pivotStatLine.getTimestamp().getTime()) {
				correctTimestamp.add(pivotStatLine.getTimestamp());
				counter++;
			}
			// We found a problem
			if(counter >= 2) {
				return correctTimestamp.get(0);
			}
		}
		return null;
	}
	
}
