package main;

import java.util.ArrayList;

/**
 * This class was built manage satellites and to accommodate extra satellites
 * Simply put, if another satellite id comes in, we create another satellite 
 * @author Ferguson
 *
 */
public class SatelliteManager {
	
	private ArrayList<Satellite> satellites = new ArrayList<>();
	
	public Satellite getSatellite(Double id) {

		for(Satellite satellite : satellites) {
			if(satellite.getId().equals(id)) {
				return satellite;
			}
		}
		Satellite newSatellite = new Satellite(id);
		satellites.add(newSatellite);

		return newSatellite;
	}
	
	public int getNumberOfSatellites() {
		return satellites.size();
	}
	
	
}
