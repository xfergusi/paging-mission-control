package main;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class is used to bring a line of the input file into an object. 
 * It parses the input file and assigns them to member variables with the proper data types
 * All values are made into double to make it easier for compares
 * @author Ferguson
 *
 * Assumptions:
 * 		All data would come in an array with 8 elements 
 *
 */
public class InputData {

	
	private Timestamp timestamp;
	private Double satelliteId;
	private Double redHighLimit;
	private Double yellowHighLimit;
	private Double yellowLowLimit;
	private Double redLowLimit;
	private Double rawValue;
	private String component;
	
	
	public InputData(String[] inputArray) {
		
		try {
		    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss.SSS");
		    Date parsedDate = dateFormat.parse(inputArray[0]);
		    this.timestamp = new java.sql.Timestamp(parsedDate.getTime());
		} catch(Exception e) {  
			System.out.println("We found a problem when converting to timestamp");
			System.out.println(e.getLocalizedMessage());
			
		}	
		
		this.satelliteId = Double.parseDouble(inputArray[1]);
		this.redHighLimit = Double.parseDouble(inputArray[2]);
		this.yellowHighLimit = Double.parseDouble(inputArray[3]);
		this.yellowLowLimit = Double.parseDouble(inputArray[4]);
		this.redLowLimit = Double.parseDouble(inputArray[5]);
		this.rawValue = Double.parseDouble(inputArray[6]);
		this.component = inputArray[7];
		
	}
	
	
	public Timestamp getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}


	public Double getSatelliteId() {
		return satelliteId;
	}


	public void setSatelliteId(Double satelliteId) {
		this.satelliteId = satelliteId;
	}


	public Double getRedHighLimit() {
		return redHighLimit;
	}


	public void setRedHighLimit(Double redHighLimit) {
		this.redHighLimit = redHighLimit;
	}


	public Double getYellowHighLimit() {
		return yellowHighLimit;
	}


	public void setYellowHighLimit(Double yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}


	public Double getYellowLowLimit() {
		return yellowLowLimit;
	}


	public void setYellowLowLimit(Double yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}


	public Double getRedLowLimit() {
		return redLowLimit;
	}


	public void setRedLowLimit(Double redLowLimit) {
		this.redLowLimit = redLowLimit;
	}


	public Double getRawValue() {
		return rawValue;
	}


	public void setRawValue(Double rawValue) {
		this.rawValue = rawValue;
	}


	public String getComponent() {
		return component;
	}


	public void setComponent(String component) {
		this.component = component;
	}



	public InputData() {
		// TODO Auto-generated constructor stub
	}
	
	

	
	
	
	
	
}
