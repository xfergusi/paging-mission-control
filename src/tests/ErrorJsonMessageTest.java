package tests;

import org.junit.jupiter.api.Test;

import main.ErrorJsonMessage;
import main.InputData;
import main.Satellite;
import main.Warning;

class ErrorJsonMessageTest {

	@Test
	void test() {
		Satellite satellite = new Satellite(2.0);
		
		InputData inputData1 = new InputData("20180101 23:01:05.001|1001|101|98|25|20|102|TSTAT".split("\\|"));
		satellite.setIssueFoundTimestamp(inputData1.getTimestamp());
		
		ErrorJsonMessage.printErrorMessage(satellite, "Test", Warning.RED_HIGH );
		
	}

}
