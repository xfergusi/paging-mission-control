package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import main.InputData;
import main.Satellite;
import main.StatLine;

class SatelliteTest {

	@Test
	void testSatellite() {
		
		Satellite satellite = new Satellite(2.0);
		assertEquals(2.0, (double) satellite.getId());
		ArrayList<StatLine> test = new ArrayList<>();
		assertEquals(test.getClass(), satellite.getBatteryStats().getClass());
	
	}

	@Test
	void testHandleStatLine() {
		Satellite satellite = new Satellite(2.0);
		
		InputData inputData1 = new InputData("20180101 23:01:05.001|1001|101|98|25|20|102|TSTAT".split("\\|"));
		StatLine statLine1 = new StatLine(inputData1);
		assertEquals(0, satellite.getThermoStats().size());
		satellite.handleStatLine(statLine1, "TSTAT");
		assertEquals(1, satellite.getThermoStats().size());
		
		InputData inputData2 = new InputData("20180101 23:01:05.001|1001|101|98|25|20|1|BATT".split("\\|"));
		StatLine statLine2 = new StatLine(inputData2);
		assertEquals(0, satellite.getBatteryStats().size());
		satellite.handleStatLine(statLine2, "BATT");
		assertEquals(1, satellite.getBatteryStats().size());
		
		
	}

}
