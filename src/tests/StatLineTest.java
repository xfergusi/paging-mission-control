/**
 * 
 */
package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import main.InputData;
import main.StatLine;
import main.Warning;

/**
 * @author Ferguson
 *
 */
class StatLineTest {
	
	static InputData inputData1;
	static InputData inputData2;
	static InputData inputData3;
	static Timestamp ts;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		inputData1 = new InputData("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT".split("\\|"));
		inputData2 = new InputData("20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT".split("\\|"));
		inputData3 = new InputData("20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT".split("\\|"));
		
		try {
		    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss.SSS");
		    Date parsedDate = dateFormat.parse("20180101 23:01:05.001");
		    ts = new java.sql.Timestamp(parsedDate.getTime());
		} catch(Exception e) {  
			System.out.println("We found a problem when converting to timestamp");
			System.out.println(e.getLocalizedMessage());
			
		}	
	
	}

	@Test
	void ConstructorTest() {
	
		StatLine statLine1 = new StatLine(inputData1);
		assertEquals(ts, statLine1.getTimestamp());
		assertEquals(Warning.NA, statLine1.getWarning());

		StatLine statLine2 = new StatLine(inputData2);
		assertEquals(Warning.RED_LOW, statLine2.getWarning());

		StatLine statLine3 = new StatLine(inputData3);
		assertEquals(Warning.RED_HIGH, statLine3.getWarning());
		
		
	}

}
