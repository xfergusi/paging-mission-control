/**
 * 
 */
package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import main.SatelliteManager;

/**
 * @author Ferguson
 *
 */
class SatelliteManagerTest {

	@Test
	void test() {
		SatelliteManager satelliteManager = new SatelliteManager();
		
		//testing creation and getting the same satellite
		satelliteManager.getSatellite(1.0);
		assertEquals(1.0, satelliteManager.getNumberOfSatellites());
		assertEquals(1.0, (double) satelliteManager.getSatellite(1.0).getId());
		
		satelliteManager.getSatellite(4.0);
		assertEquals(2.0, satelliteManager.getNumberOfSatellites());
		assertEquals(4.0, (double) satelliteManager.getSatellite(4.0).getId());
		

		
	}

}
