package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import main.InputData;
import main.Satellite;
import main.StatLine;

class ProblemCheckerTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@Test
	void testCheckForIssues() {
		Satellite satellite = new Satellite(2.0);
		assertNull(satellite.getIssueFoundTimestamp());
		//alert
		StatLine statLine1 = new StatLine(new InputData("20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT".split("\\|")));
		satellite.handleStatLine(statLine1, "BATT");
		assertNull(satellite.getIssueFoundTimestamp());
		//alert
		StatLine statLine2 = new StatLine(new InputData("20180101 23:01:12.522|1000|17|15|9|8|7.8|BATT".split("\\|")));
		satellite.handleStatLine(statLine2, "BATT");
		assertNull(satellite.getIssueFoundTimestamp());
		//not an alert
		StatLine statLine3 = new StatLine(new InputData("20180101 23:01:12.532|1000|17|15|9|8|8.1|BATT".split("\\|")));
		satellite.handleStatLine(statLine3, "BATT");
		assertNull(satellite.getIssueFoundTimestamp());
		//alert but out of the time range
		StatLine statLine4 = new StatLine(new InputData("20180101 23:14:12.532|1000|17|15|9|8|7.8|BATT".split("\\|")));
		satellite.handleStatLine(statLine4, "BATT");
		assertNull(satellite.getIssueFoundTimestamp());
		//alert to trigger a problem
		StatLine statLine5 = new StatLine(new InputData("20180101 23:01:15.523|1000|17|15|9|8|7.8|BATT".split("\\|")));
		satellite.handleStatLine(statLine5, "BATT");
		assertNotNull(satellite.getIssueFoundTimestamp());
		satellite.setIssueFoundTimestamp(null);
		//alert for different time
		StatLine statLine6 = new StatLine(new InputData("20180101 23:15:15.523|1000|17|15|9|8|7.8|BATT".split("\\|")));
		satellite.handleStatLine(statLine6, "BATT");
		assertNull(satellite.getIssueFoundTimestamp());
		//alert to trigger another problem for different time
		StatLine statLine7 = new StatLine(new InputData("20180101 23:15:16.523|1000|17|15|9|8|7.8|BATT".split("\\|")));
		satellite.handleStatLine(statLine7, "BATT");
		assertNotNull(satellite.getIssueFoundTimestamp());
		satellite.setIssueFoundTimestamp(null);
		//different component to make sure we don't break anything
		StatLine statLine8 = new StatLine(new InputData("20180101 23:15:16.523|1000|17|15|9|8|7.8|FSTAT".split("\\|")));
		satellite.handleStatLine(statLine8, "BATT");
		assertNotNull(satellite.getIssueFoundTimestamp());
	}

}
