package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import main.InputData;

class InputDataTest {

	@Test
	void testInputDataStringArray() {
		String[] intArray = new String[]{ "20180101 23:01:05.001","2","3","4","5","6","7","8" };
		InputData inputData = new InputData(intArray);
		assertEquals(inputData.getTimestamp().toString(), "2018-01-01 23:01:05.001");
	}

}
